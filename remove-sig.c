/*
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gnutls/gnutls.h>
#include <gnutls/pkcs7.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include "p7v.h"

int remove_sig_file(const char *filename, sign_st * s)
{
	off_t filesize;
	char buf[512];
	int ret, fd = -1;
	uint32_t siglen;
	module_sig_st sig_st;
	int e;

	fd = open(filename, O_RDWR);
	if (fd == -1) {
		e = errno;
		fprintf(stderr, "Could not load file: %s: %s\n", filename,
			strerror(e));
		goto fail;
	}

	filesize = lseek(fd, 0, SEEK_END);
	if (filesize == -1) {
		e = errno;
		fprintf(stderr, "Error seeking file: %s\n", strerror(e));
		goto fail;
	}

	if (filesize < sizeof(module_sig_st) + magic_number_size) {
		goto sig_fail;
	}

	lseek(fd, filesize - magic_number_size, SEEK_SET);
	ret = read(fd, buf, magic_number_size);
	if (ret != magic_number_size) {
		goto sig_fail;
	}

	if (memcmp(buf, magic_number, magic_number_size) != 0) {
		goto sig_fail;
	}

	lseek(fd, filesize - magic_number_size - sizeof(module_sig_st),
	      SEEK_SET);
	ret = read(fd, &sig_st, sizeof(module_sig_st));
	if (ret != sizeof(module_sig_st)) {
		goto sig_fail;
	}

	if (sig_st.id_type != PKEY_ID_PKCS7) {
		fprintf(stderr,
			"The signature type of this file (%d) is unsupported\n",
			(int)sig_st.id_type);
		goto fail;
	}

	memcpy(&siglen, &sig_st.sig_len, 4);
	siglen = ntohl(siglen);

	if (filesize < sizeof(module_sig_st) + magic_number_size + siglen) {
		fprintf(stderr, "File contains garbled signature data\n");
		goto fail;
	}

	lseek(fd, 0, SEEK_SET);
	ret =
	    ftruncate(fd,
		      filesize - (sizeof(module_sig_st) + magic_number_size +
				  siglen));
	if (ret == -1) {
		e = errno;
		fprintf(stderr, "Error truncating file: %s\n", strerror(e));
		goto fail;
	}

	close(fd);
	return 0;
 sig_fail:
	fprintf(stderr, "File doesn't contain signature data\n");
 fail:
	if (fd != -1)
		close(fd);
	return 1;
}
