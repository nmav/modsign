CC=gcc
CFLAGS=-O2 -g -Wall
LDFLAGS=-L/usr/local/lib -lgnutls

all: modsign

modsign: main.c sign.c verify.c common.c info.c remove-sig.c
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

check: modsign
	./run-tests

clean:
	rm -f modsign *.o
