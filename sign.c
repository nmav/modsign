/*
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gnutls/gnutls.h>
#include <gnutls/pkcs7.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <limits.h>
#include <arpa/inet.h>
#include "p7v.h"

static
int store_sig(const char *filename, gnutls_datum_t * sig, sign_st * s)
{
	FILE *fp = NULL;
	module_sig_st msig;
	char sigfile[_POSIX_PATH_MAX];

	if (s->detached == 0) {
		fp = fopen(filename, "a");
		if (fp == NULL)
			return 1;

		memset(&msig, 0, sizeof(msig));
		msig.id_type = PKEY_ID_PKCS7;

		msig.sig_len = htonl(sig->size);

		if (fwrite(sig->data, 1, sig->size, fp) != sig->size)
			return 1;

		if (fwrite(&msig, 1, sizeof(msig), fp) != sizeof(msig))
			return 1;

		if (fwrite(magic_number, 1, magic_number_size, fp) != magic_number_size)
			return 1;
	} else {
		snprintf(sigfile, sizeof(sigfile), "%s.p7s", filename);

		fp = fopen(sigfile, "w");
		if (fp == NULL)
			return 1;

		if (fwrite(sig->data, 1, sig->size, fp) != sig->size)
			return 1;

		fclose(fp);
	}

	return 0;
}

static
int replace_sig(const char *filename, gnutls_datum_t * sig, sign_st * s)
{
	if (s->detached == 0) {
		if (remove_sig_file(filename, s) != 0)
			return 1;
	}

	return store_sig(filename, sig, s);
}

static
unsigned make_flags(sign_st *s)
{
	unsigned flags = 0;
	if (s->include_cert)
		flags |= GNUTLS_PKCS7_INCLUDE_CERT;

	if (s->include_time)
		flags |= GNUTLS_PKCS7_INCLUDE_TIME;

	if (s->include_spki)
		flags |= GNUTLS_PKCS7_WRITE_SPKI;

	return flags;
}

int pkcs7_sign(gnutls_pkcs7_t p7, gnutls_datum_t *data, sign_st * s)
{
	gnutls_x509_crt_t crt = NULL;
	gnutls_privkey_t privkey = NULL;
	int ret;

	if (s->key_file == NULL) {
		fprintf(stderr,
			"No key file has been specified to sign with.\n");
		return 1;
	}

	if (s->signer_file == NULL) {
		fprintf(stderr,
			"No signer file has been specified to sign with.\n");
		return 1;
	}

	crt = load_crt(s);
	if (crt == NULL) {
		return 1;
	}

	privkey = load_privkey(s);
	if (privkey == NULL) {
		ret = 1;
		goto cleanup;
	}

	ret = gnutls_pkcs7_sign(p7, crt, privkey, data, NULL, NULL, s->digest,
				make_flags(s));
	if (ret < 0) {
		fprintf(stderr, "Could not sign data: %s\n",
			gnutls_strerror(ret));
		ret = 1;
		goto cleanup;
	}

	ret = 0;
 cleanup:
 	if (crt)
	 	gnutls_x509_crt_deinit(crt);
	if (privkey)
		gnutls_privkey_deinit(privkey);
	return ret;
}


int sign_file(const char *filename, sign_st * s)
{
	gnutls_datum_t data = { NULL, 0 };
	gnutls_datum_t sig = { NULL, 0 };
	gnutls_pkcs7_t p7 = NULL;
	int ret;

	ret = gnutls_load_file(filename, &data);
	if (ret < 0) {
		fprintf(stderr, "Couldn't load file: %s\n", filename);
		ret = 1;
		goto cleanup;
	}

	/* check if the file is already signed */
	if (data.size > magic_number_size
	    && memcmp(&data.data[data.size - magic_number_size], magic_number,
		      magic_number_size) == 0) {
		fprintf(stderr, "The file is already signed\n");
		ret = 1;
		goto cleanup;
	}

	ret = gnutls_pkcs7_init(&p7);
	if (ret < 0) {
		fprintf(stderr, "Error initializing pkcs7 struct: %s\n",
			gnutls_strerror(ret));
		ret = 1;
		goto cleanup;
	}

	ret = pkcs7_sign(p7, &data, s);
	if (ret != 0) {
		ret = 1;
		goto cleanup;
	}

	ret = gnutls_pkcs7_export2(p7, GNUTLS_X509_FMT_DER, &sig);
	if (ret < 0) {
		fprintf(stderr, "Could not export signed data: %s\n",
			gnutls_strerror(ret));
		ret = 1;
		goto cleanup;
	}

	/* store the signed data */
	ret = store_sig(filename, &sig, s);
	if (ret != 0) {
		fprintf(stderr, "Could not store signed data\n");
		ret = 1;
		goto cleanup;
	}

	ret = 0;
 cleanup:
	gnutls_free(data.data);
	gnutls_free(sig.data);
	if (p7)
		gnutls_pkcs7_deinit(p7);

	return ret;
}

int add_sig_file(const char *filename, sign_st * s)
{
	int ret;
	file_st f;
	gnutls_datum_t psig, pdata;
	gnutls_datum_t sig = { NULL, 0 };
	gnutls_pkcs7_t p7 = NULL;

	ret = load_file(filename, s, &f);
	if (ret != 0) {
		return 1;
	}

	ret = gnutls_pkcs7_init(&p7);
	if (ret < 0) {
		fprintf(stderr, "Error initializing pkcs7 struct: %s\n",
			gnutls_strerror(ret));
		ret = 1;
		goto cleanup;
	}

	psig.data = f.sig;
	psig.size = f.sig_size;
	ret = gnutls_pkcs7_import(p7, &psig, GNUTLS_X509_FMT_DER);
	if (ret < 0) {
		fprintf(stderr, "Error importing the existing PKCS #7 data: %s\n",
			gnutls_strerror(ret));
		ret = 1;
		goto cleanup;
	}

	pdata.data = f.contents;
	pdata.size = f.contents_size;
	ret = pkcs7_sign(p7, &pdata, s);
	if (ret != 0) {
		ret = 1;
		goto cleanup;
	}

	ret = gnutls_pkcs7_export2(p7, GNUTLS_X509_FMT_DER, &sig);
	if (ret < 0) {
		fprintf(stderr, "Could not export signed data: %s\n",
			gnutls_strerror(ret));
		ret = 1;
		goto cleanup;
	}

	unload_file(&f);

	/* store the signed data */
	ret = replace_sig(filename, &sig, s);
	if (ret != 0) {
		fprintf(stderr, "Could not store signed data\n");
		ret = 1;
		goto cleanup;
	}

	ret = 0;
 cleanup:
	unload_file(&f); /* we can do it multiple times */
	gnutls_free(sig.data);
	if (p7 != NULL)
		gnutls_pkcs7_deinit(p7);
	return ret;
}
