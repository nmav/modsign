/*
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gnutls/gnutls.h>
#include <gnutls/pkcs7.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "p7v.h"

static
int info_mem(uint8_t * data, size_t data_size, sign_st * st)
{
	gnutls_pkcs7_t p7;
	gnutls_datum_t d = { data, data_size }, out;
	int ret;

	ret = gnutls_pkcs7_init(&p7);
	if (ret < 0)
		return 1;

	ret = gnutls_pkcs7_import(p7, &d, GNUTLS_X509_FMT_DER);
	if (ret < 0) {
		fprintf(stderr, "Cannot import PKCS #7 data: %s\n",
			gnutls_strerror(ret));
		goto fail;
	}

	ret =
	    gnutls_pkcs7_print(p7,
			       st->
			       verbose ? GNUTLS_CRT_PRINT_FULL :
			       GNUTLS_CRT_PRINT_COMPACT, &out);
	if (ret < 0) {
		fprintf(stderr, "Cannot print PKCS #7 data: %s\n",
			gnutls_strerror(ret));
		goto fail;
	}
	printf("%s", out.data);
	gnutls_free(out.data);

	gnutls_pkcs7_deinit(p7);
	return 0;

 fail:
	gnutls_pkcs7_deinit(p7);
	return 1;

}

int info_file(const char *filename, sign_st * s)
{
	FILE *fp = NULL;
	ssize_t filesize;
	char buf[512];
	uint8_t *filemap;
	int ret, fd;
	uint32_t siglen;
	module_sig_st sig_st;

	fp = fopen(filename, "r");
	if (fp == NULL) {
		fprintf(stderr, "Could not load signed file: %s\n", filename);
		goto fail;
	}

	filesize = fseek(fp, 0, SEEK_END);
	if (filesize == -1) {
		fprintf(stderr, "Error seeking file\n");
		goto fail;
	}
	filesize = ftell(fp);	/* fix our precision */

	if (filesize < sizeof(module_sig_st) + magic_number_size) {
		goto sig_fail;
	}

	fseek(fp, filesize - magic_number_size, SEEK_SET);
	ret = fread(buf, 1, magic_number_size, fp);
	if (ret != magic_number_size) {
		goto sig_fail;
	}

	if (memcmp(buf, magic_number, magic_number_size) != 0) {
		goto sig_fail;
	}

	fseek(fp, filesize - magic_number_size - sizeof(module_sig_st),
	      SEEK_SET);
	ret = fread(&sig_st, 1, sizeof(module_sig_st), fp);
	if (ret != sizeof(module_sig_st)) {
		goto sig_fail;
	}

	if (sig_st.id_type != PKEY_ID_PKCS7) {
		fprintf(stderr,
			"The signature type of this file (%d) is unsupported\n",
			(int)sig_st.id_type);
		goto fail;
	}

	memcpy(&siglen, &sig_st.sig_len, 4);
	siglen = ntohl(siglen);

	if (filesize < sizeof(module_sig_st) + magic_number_size + siglen) {
		fprintf(stderr, "File contains garbled signature data\n");
		goto fail;
	}

	fd = fileno(fp);

	filemap = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
	if (filemap == MAP_FAILED) {
		fprintf(stderr, "Error mapping file\n");
		goto fail;
	}

	ret =
	    info_mem(&filemap
		     [filesize -
		      (sizeof(module_sig_st) + magic_number_size + siglen)],
		     siglen, s);

	munmap(filemap, filesize);
	fclose(fp);
	return ret;
 sig_fail:
	fprintf(stderr, "File doesn't contain signature data\n");
 fail:
	if (fp != NULL)
		fclose(fp);
	return 1;
}
