/*
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gnutls/gnutls.h>
#include <gnutls/pkcs7.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "p7v.h"

static
int verify_mem(gnutls_x509_crt_t signer, uint8_t * data, size_t data_size,
	       uint8_t * sig, size_t sig_size, sign_st * st)
{
	gnutls_pkcs7_t p7;
	gnutls_datum_t d = { data, data_size };
	gnutls_datum_t s = { sig, sig_size };
	int ret, saved_ret = GNUTLS_E_INTERNAL_ERROR;
	gnutls_datum_t dn = {NULL, 0};
	int retval = 1;
	unsigned idx;

	ret = gnutls_pkcs7_init(&p7);
	if (ret < 0)
		return 1;

	ret = gnutls_pkcs7_import(p7, &s, GNUTLS_X509_FMT_DER);
	if (ret < 0) {
		fprintf(stderr, "Cannot import PKCS #7 data: %s\n",
			gnutls_strerror(ret));
		goto fail;
	}

	/* verify */
	for (idx = 0;; idx++) {
		if (signer) {
			ret = gnutls_x509_crt_get_dn2(signer, &dn);
			if (ret < 0) {
				fprintf(stderr, "Cannot read name of signer: %s\n", gnutls_strerror(ret));
				goto fail;
			}

			ret = gnutls_pkcs7_verify_direct(p7, signer, idx, &d, 0);
		} else {
			gnutls_x509_trust_list_t tl;
			gnutls_typed_vdata_st vdata =
			    { GNUTLS_DT_KEY_PURPOSE_OID,
			      (void *)GNUTLS_KP_CODE_SIGNING, 0
			    };

			ret = gnutls_x509_trust_list_init(&tl, 0);
			if (ret < 0) {
				fprintf(stderr,
					"Cannot initialize trust list: %s\n",
					gnutls_strerror(ret));
				goto fail;
			}

			ret = gnutls_x509_trust_list_add_system_trust(tl, 0, 0);
			if (ret < 0) {
				fprintf(stderr,
					"Cannot import system trust list: %s\n",
					gnutls_strerror(ret));
				gnutls_x509_trust_list_deinit(tl, 0);
				goto fail;
			}

			ret = gnutls_pkcs7_verify(p7, tl, &vdata, 1, idx, &d, 0);
			gnutls_x509_trust_list_deinit(tl, 0);
		}

		if (ret == GNUTLS_E_REQUESTED_DATA_NOT_AVAILABLE) {
			ret = saved_ret;
			break;
		} else if (ret >= 0) {
			break;
		}
		saved_ret = ret; /* try next one */
	}

	if (ret < 0) {
		fprintf(stderr, "Error in verification: %s\n",
			gnutls_strerror(ret));
		goto fail;
	}

	if (dn.data)
		printf("Good signature from '%s'\n", dn.data);
	else
		printf("Good signature for code signing\n");

	retval = 0;

 fail:
	gnutls_free(dn.data);
	gnutls_pkcs7_deinit(p7);
	return retval;

}

int verify_file(const char *filename, sign_st * s)
{
	gnutls_x509_crt_t crt = NULL;
	int ret;
	file_st f;

	if (s->signer_file) {
		crt = load_crt(s);
		if (crt == NULL) {
			return 1;
		}
	}

	ret = load_file(filename, s, &f);
	if (ret != 0) {
		goto fail;
	}

	ret = verify_mem(crt,
			 f.contents, f.contents_size, f.sig, f.sig_size, s);

	unload_file(&f);
	if (crt)
		gnutls_x509_crt_deinit(crt);
	return ret;
 fail:
	if (crt)
		gnutls_x509_crt_deinit(crt);
	return 1;
}
