#ifndef P7V_H
#define P7V_H

#include <stdint.h>
#include <stdbool.h>
#include <gnutls/gnutls.h>

typedef struct module_sig_st {
	uint8_t algo;
	uint8_t hash;
	uint8_t id_type;
	uint8_t signer_len;
	uint8_t key_id_len;
	uint8_t __pad[3];
	uint32_t sig_len;
} __attribute__ ((__packed__)) module_sig_st;

#define PKEY_ID_PKCS7 2

extern char magic_number[];
extern const unsigned magic_number_size;

typedef struct sign_st {
	const char *signer_file;
	const char *key_file;
	unsigned digest;
	bool verbose;
	bool batch;
	bool include_cert;
	bool include_time;
	bool include_spki;
	bool detached;
} sign_st;

typedef struct file_st {
	void *contents;
	size_t contents_size;

	void *sig;
	size_t sig_size;
	bool sig_is_mapped;

	module_sig_st msig;

	void *map_data;
	size_t map_size;
} file_st;

int load_file(const char *filename, sign_st *s, file_st *f);
void unload_file(file_st *f);

int sign_file(const char *filename, sign_st * s);
int add_sig_file(const char *filename, sign_st * s);
int verify_file(const char *filename, sign_st * s);
int info_file(const char *filename, sign_st * s);
int remove_sig_file(const char *filename, sign_st * s);

gnutls_x509_crt_t load_crt(sign_st * s);
gnutls_privkey_t load_privkey(sign_st * s);

int
pin_callback(void *user, int attempt, const char *token_url,
             const char *token_label, unsigned int flags, char *pin,
             size_t pin_max);

#endif
