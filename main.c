/*
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>
#include <gnutls/pkcs11.h>
#include "p7v.h"

enum {
	OPT_INCLUDE_CERT=300,
	OPT_INCLUDE_TIME,
	OPT_INCLUDE_SPKI,
	OPT_ADD_SIG,
	OPT_DIGEST,
	OPT_DETACHED
};

const static struct option long_options[] = {
	{"verify", no_argument, 0, 'v'},
	{"verbose", no_argument, 0, 'V'},
	{"batch", no_argument, 0, 'b'},
	{"sign", no_argument, 0, 's'},
	{"add-sig", no_argument, 0, OPT_ADD_SIG},
	{"remove-sig", no_argument, 0, 'r'},
	{"siginfo", no_argument, 0, 'i'},
	{"include-cert", no_argument, 0, OPT_INCLUDE_CERT},
	{"include-spki", no_argument, 0, OPT_INCLUDE_SPKI},
	{"include-timestamp", no_argument, 0, OPT_INCLUDE_TIME},
	{"detached", no_argument, 0, OPT_DETACHED},
	{"signer", required_argument, 0, 'c'},
	{"key", required_argument, 0, 'k'},
	{"digest", required_argument, 0, OPT_DIGEST},
	{"help", no_argument, 0, 'h'},
	{0, 0, 0, 0}
};

static void usage(const char *arg)
{
	fprintf(stderr, "%s: a tool to sign and verify files.\n\n", arg);
	fprintf(stderr, "Usage: %s [COMMAND] [OPTIONS] [FILE]\n", arg);
	fprintf(stderr, "Commands:\n");
	fprintf(stderr, "  -s  --sign                         Sign the file.\n");
	fprintf(stderr, "      --add-sig                      Append a signature to the file.\n");
	fprintf(stderr, "  -v  --verify                       Verify the signature in the file.\n");
	fprintf(stderr, "  -i  --siginfo                      Print info on the signature.\n");
	fprintf(stderr, "  -r  --remove-sig                   Remove any signature.\n");
	fprintf(stderr, "\nOptions:\n");
	fprintf(stderr, "  -c  --signer [PEM_FILE|URL]        Specify signer.\n");
	fprintf(stderr, "  -k  --key    [PEM_FILE|URL]        Specify the signing key.\n");
	fprintf(stderr, "      --digest [sha1|sha256|sha512]  Set the digest algorithm.\n");
	fprintf(stderr, "      --include-cert                 Include the signing certificate in signature.\n");
	fprintf(stderr, "      --include-timestamp            Include a timestamp in signature.\n");
	fprintf(stderr, "      --include-spki                 Include the key identifier of the signer.\n");
	fprintf(stderr, "      --detached                     The signature will be placed in a separate .p7s file.\n");
	fprintf(stderr, "  -V  --verbose                      Print more verbose output\n");
	fprintf(stderr, "  -b  --batch                        Force non-interactive mode.\n");
	fprintf(stderr, "\nExample:\n");
	fprintf(stderr,
		"   %s --verify --certificate my-signer.pem module.ko\n", arg);
}

typedef enum operation_type {
	OP_NONE = 0,
	OP_SIGN,
	OP_REMOVE_SIG,
	OP_SIGINFO,
	OP_ADD_SIG,
	OP_VERIFY
} operation_type;

int main(int argc, char **argv)
{
	int c, option_index = 0;
	operation_type op = OP_NONE;
	sign_st st;

	memset(&st, 0, sizeof(st));
	gnutls_pkcs11_set_pin_function(pin_callback, &st);

	while (1) {
		c = getopt_long(argc, argv, "Vridc:k:vsh?", long_options,
				&option_index);
		if (c == -1) {
			break;
		}

		st.digest = GNUTLS_DIG_SHA256;

		switch (c) {
		case 'v':
			if (op != OP_NONE)
				goto fail;
			op = OP_VERIFY;
			break;
		case 's':
			if (op != OP_NONE)
				goto fail;
			op = OP_SIGN;
			break;
		case OPT_ADD_SIG:
			if (op != OP_NONE)
				goto fail;
			op = OP_ADD_SIG;
			break;
		case 'r':
			if (op != OP_NONE)
				goto fail;
			op = OP_REMOVE_SIG;
			break;
		case 'i':
			if (op != OP_NONE)
				goto fail;
			op = OP_SIGINFO;
			break;
		case 'c':
			st.signer_file = optarg;
			break;
		case 'k':
			st.key_file = optarg;
			break;
		case OPT_DIGEST:
			if (strcmp(optarg, "sha1") == 0)
				st.digest = GNUTLS_DIG_SHA1;
			else if (strcmp(optarg, "sha256") == 0
				 || strcmp(optarg, "sha2-256") == 0)
				st.digest = GNUTLS_DIG_SHA256;
			else if (strcmp(optarg, "sha512") == 0
				 || strcmp(optarg, "sha2-512") == 0)
				st.digest = GNUTLS_DIG_SHA512;
			else {
				fprintf(stderr, "Unknown digest: %s\n", optarg);
				return 1;
			}
			break;
		case 'V':
			st.verbose = 1;
			break;
		case 'b':
			st.batch = 1;
			break;
		case OPT_DETACHED:
			st.detached = 1;
			break;
		case OPT_INCLUDE_CERT:
			st.include_cert = 1;
			break;
		case OPT_INCLUDE_TIME:
			st.include_time = 1;
			break;
		case OPT_INCLUDE_SPKI:
			if (gnutls_check_version("3.4.3") == NULL) {
				fprintf(stderr, "The spki option is not available with this version of GnuTLS\n");
				return 1;
			}
			st.include_spki = 1;
			break;
		case '?':
		case 'h':
			usage(argv[0]);
			return 0;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "Expecting filename\n\n");
		usage(argv[0]);
		return 1;
	}

	switch (op) {
	case OP_REMOVE_SIG:
		return remove_sig_file(argv[optind], &st);
	case OP_SIGN:
		return sign_file(argv[optind], &st);
	case OP_ADD_SIG:
		return add_sig_file(argv[optind], &st);
	case OP_SIGINFO:
		return info_file(argv[optind], &st);
	case OP_VERIFY:
		return verify_file(argv[optind], &st);
	default:
		usage(argv[0]);
		return 1;
	}

	return 0;
 fail:
	usage(argv[0]);
	return 1;
}
