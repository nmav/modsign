Information about modsign
=========================

This is a command line tool to sign and verify files using PKCS #7 signatures.

It requires GnuTLS 3.4.2.
