/*
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gnutls/gnutls.h>
#include <gnutls/pkcs7.h>
#include <gnutls/x509.h>
#include <gnutls/pkcs11.h>
#include <gnutls/abstract.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>
#include "p7v.h"

char magic_number[] = "~Module signature appended~\n";
const unsigned magic_number_size = sizeof(magic_number) - 1;

gnutls_x509_crt_t load_crt(sign_st * s)
{
	int ret;
	gnutls_datum_t data = { NULL, 0 };
	gnutls_x509_crt_t crt = NULL;

	ret = gnutls_x509_crt_init(&crt);
	if (ret < 0) {
		fprintf(stderr, "Error initializing certificate\n");
		goto fail;
	}

	if (gnutls_url_is_supported(s->signer_file)) {
		ret = gnutls_x509_crt_import_pkcs11_url(crt, s->signer_file, 0);
		if (ret < 0) {
			fprintf(stderr, "Couldn't load URL: %s\n",
				s->signer_file);
			goto fail;
		}
	} else {
		ret = gnutls_load_file(s->signer_file, &data);
		if (ret < 0) {
			fprintf(stderr, "Couldn't load file: %s\n",
				s->signer_file);
			goto fail;
		}

		ret = gnutls_x509_crt_import(crt, &data, GNUTLS_X509_FMT_PEM);
		if (ret < 0) {
			fprintf(stderr, "Error loading signer certificate\n");
			goto fail;
		}
	}

	gnutls_free(data.data);
	return crt;
 fail:
	if (crt != NULL)
		gnutls_x509_crt_deinit(crt);
	gnutls_free(data.data);
	return NULL;
}

gnutls_privkey_t load_privkey(sign_st * s)
{
	gnutls_privkey_t privkey;
	gnutls_datum_t data = { NULL, 0 };
	char *pass;
	int ret;

	ret = gnutls_privkey_init(&privkey);
	if (ret < 0)
		return NULL;

	if (gnutls_url_is_supported(s->key_file)) {
		ret = gnutls_privkey_import_url(privkey, s->key_file, 0);
	} else {
		ret = gnutls_load_file(s->key_file, &data);
		if (ret < 0) {
			fprintf(stderr, "Couldn't load key file: %s\n",
				s->key_file);
			goto fail;
		}

		ret =
		    gnutls_privkey_import_x509_raw(privkey, &data,
						   GNUTLS_X509_FMT_PEM, NULL,
						   0);
		if (ret == GNUTLS_E_DECRYPTION_FAILED && s->batch == 0) {
			fprintf(stderr, "This file is password protected.\n");
			pass = getpass("Enter password:");
			ret =
			    gnutls_privkey_import_x509_raw(privkey, &data,
							   GNUTLS_X509_FMT_PEM,
							   pass, 0);
		}
		if (ret < 0) {
			fprintf(stderr, "Cannot read key: %s\n",
				gnutls_strerror(ret));
			goto fail;
		}
	}

	return privkey;
 fail:
	gnutls_privkey_deinit(privkey);
	return NULL;
}

#define MIN(x,y) ((x)<(y))?(x):(y)
#define MAX_CACHE_TRIES 3
int
pin_callback(void *user, int attempt, const char *token_url,
	     const char *token_label, unsigned int flags, char *pin,
	     size_t pin_max)
{
	const char *password = NULL;
	sign_st *sig_info = user;
	const char *desc;
	int cache = MAX_CACHE_TRIES;
	unsigned len;
/* allow caching of PIN */
	static char *cached_url = NULL;
	static char cached_pin[32] = "";
	const char *env;

	if (flags & GNUTLS_PIN_SO) {
		env = "GNUTLS_SO_PIN";
		desc = "security officer";
	} else {
		env = "GNUTLS_PIN";
		desc = "user";
	}

	if (flags & GNUTLS_PIN_FINAL_TRY) {
		cache = 0;
		printf("*** This is the final try before locking!\n");
	}
	if (flags & GNUTLS_PIN_COUNT_LOW) {
		cache = 0;
		printf("*** Only few tries left before locking!\n");
	}

	if (flags & GNUTLS_PIN_WRONG) {
		cache = 0;
		printf("*** Wrong PIN has been provided!\n");
	}

	if (cache > 0 && cached_url != NULL) {
		if (token_url != NULL
		    && strcmp(cached_url, token_url) == 0) {
			if (strlen(cached_pin) >= pin_max) {
				fprintf(stderr, "Too long PIN given\n");
				exit(1);
			}

			fprintf(stderr,
				"Re-using cached PIN for token '%s'\n",
				token_label);
			strcpy(pin, cached_pin);
			cache--;
			return 0;
		}
	}

	printf("Token '%s' with URL '%s' ", token_label, token_url);
	printf("requires %s PIN\n", desc);

	password = getenv(env);
	if (password == NULL) /* compatibility */
		password = getenv("GNUTLS_PIN");

	if (password == NULL && (sig_info == NULL || sig_info->batch == 0)) {
		password = getpass("Enter PIN: ");
	} else {
		if (flags & GNUTLS_PIN_WRONG) {
			fprintf(stderr, "Cannot continue with a wrong password in the environment.\n");
			exit(1);
		}
	}

	if (password == NULL || password[0] == 0 || password[0] == '\n') {
		fprintf(stderr, "No PIN given.\n");
		if (sig_info != NULL && sig_info->batch != 0) {
			fprintf(stderr, "note: when operating in batch mode, set the GNUTLS_PIN or GNUTLS_SO_PIN environment variables\n");
		}
		exit(1);
	}

	len = MIN(pin_max - 1, strlen(password));
	memcpy(pin, password, len);
	pin[len] = 0;

	/* cache */
	if (len < sizeof(cached_pin)) {
		memcpy(cached_pin, pin, len);
		cached_pin[len] = 0;
	} else
		cached_pin[0] = 0;

	free(cached_url);
	if (token_url)
		cached_url = strdup(token_url);
	else
		cached_url = NULL;

	cache = MAX_CACHE_TRIES;

	return 0;
}

int load_file(const char *filename, sign_st *s, file_st *f)
{
	ssize_t filesize;
	char buf[512];
	int ret, fd = -1;
	uint32_t siglen = 0;
	int e;
	uint8_t *filemap;
	gnutls_datum_t sigdata = {NULL,0};

	memset(f, 0, sizeof(*f));

	fd = open(filename, O_RDWR);
	if (fd == -1) {
		e = errno;
		fprintf(stderr, "Could not load file: %s: %s\n", filename,
			strerror(e));
		goto fail;
	}

	filesize = lseek(fd, 0, SEEK_END);
	if (filesize == -1) {
		e = errno;
		fprintf(stderr, "Error seeking file: %s\n", strerror(e));
		goto fail;
	}

	if (s->detached == 0) {
		if (filesize < sizeof(module_sig_st) + magic_number_size) {
			goto sig_fail;
		}

		lseek(fd, filesize - magic_number_size, SEEK_SET);
		ret = read(fd, buf, magic_number_size);
		if (ret != magic_number_size) {
			goto sig_fail;
		}

		if (memcmp(buf, magic_number, magic_number_size) != 0) {
			goto sig_fail;
		}

		lseek(fd, filesize - magic_number_size - sizeof(module_sig_st),
		      SEEK_SET);
		ret = read(fd, &f->msig, sizeof(module_sig_st));
		if (ret != sizeof(module_sig_st)) {
			goto sig_fail;
		}

		if (f->msig.id_type != PKEY_ID_PKCS7) {
			fprintf(stderr,
				"The signature type of this file (%d) is unsupported\n",
				(int)f->msig.id_type);
			goto fail;
		}

		memcpy(&siglen, &f->msig.sig_len, 4);
		siglen = ntohl(siglen);

		if (filesize < sizeof(module_sig_st) + magic_number_size + siglen) {
			fprintf(stderr, "File contains garbled signature data\n");
			goto fail;
		}
	} else {
		/* read detached signature */
		char sigfile[_POSIX_PATH_MAX];
		snprintf(sigfile, sizeof(sigfile), "%s.p7s", filename);

		ret = gnutls_load_file(sigfile, &sigdata);
		if (ret < 0) {
			fprintf(stderr, "Could not read detached signature file\n");
			goto fail;
		}
	}

	f->map_size = filesize;

	filemap = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
	if (filemap == MAP_FAILED) {
		fprintf(stderr, "Error mapping file\n");
		goto fail;
	}

	f->map_data = filemap;
	f->contents = filemap;

	if (s->detached == 0) {
		f->contents_size = filesize - (sizeof(module_sig_st) + magic_number_size + siglen);
		f->sig = &filemap[filesize - (sizeof(module_sig_st) + magic_number_size + siglen)];
		f->sig_size = siglen;
		f->sig_is_mapped = 1;
	} else {
		f->contents_size = filesize;
		f->sig = sigdata.data;
		f->sig_size = sigdata.size;
		f->sig_is_mapped = 0;
	}

	close(fd);

	return 0;
 sig_fail:
	fprintf(stderr, "File doesn't contain signature data\n");
 fail:
	if (fd != -1)
		close(fd);
	return 1;
}

void unload_file(file_st *f)
{
	if (f->map_data != NULL && f->map_size > 0)
		munmap(f->map_data, f->map_size);
	f->map_data = NULL;
	f->map_size = 0;

	if (f->sig_is_mapped == 0) {
		gnutls_free(f->sig);
	}

	f->sig = NULL;
	f->sig_size = 0;
}
